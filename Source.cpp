#include <iostream>
#include <string>

using namespace std;

void primeNum(int num, int index)
{
	if (num <= 0)
		return;
	
	if (index != 1)
	{
		if (index == 2 || index == 3 )
			cout << index << "  ";

		else if (index % 2 != 0 && index % 3 != 0)
			cout << index << "  ";
	}

	primeNum(num - 1, index + 1);
}

void fibonacci(int totalOutput, int prevNum, int nextNum, int sum)
{
	if (totalOutput <= 0)
		return;

	sum = prevNum + nextNum;

	cout << sum << "  ";

	nextNum = prevNum;
	prevNum = sum;

	fibonacci(totalOutput - 1, prevNum, nextNum, sum + 1);
}

int computeSum(int totalOutput, int sum)
{
	int num = 0;

	if (totalOutput <= 0)
		return sum;

	cin >> num;
	sum = sum + num;

	computeSum(totalOutput - 1, sum);
}

int main()
{
	int num = 0;
	int sum = 0;
	int choice = 0;

	cout << "What would you like to do? \n" << "[1] Compute the sum of numbers \n" << "[2] Print Fibonacci numbers \n" << "[3] Check if a number is prime or not \n";
	cin >> choice;

	if (choice != 1 && choice != 2 && choice != 3	)
	{
		cout << "\nPlease try again";
		cin >> choice;
	}
	else if (choice == 1)
	{
		cout << "\nHow many numbers do you want to compute for? \n";
		cin >> num;
		
		cout << "\nEnter " << num << " numbers \n";
		computeSum(num, sum);

		cout << "\nSum: " << sum;
	}
	else if (choice == 2)
	{
		int prevNum = 1;
		int nextNum = 0;

		cout << "\nPick a number: ";
		cin >> num;

		cout << "\nFibonacci Sequence: ";
		fibonacci(num, prevNum, nextNum, sum);
	}
	else if (choice == 3)
	{
		int index = 0;

		cout << "\nHow many numbers do you want to check? \n";
		cin >> num;

		cout << "\nPrime Numbers: ";
		primeNum(num, index);
	}

	cout << "\n" << endl;
	system("pause");
	system("cls");
}